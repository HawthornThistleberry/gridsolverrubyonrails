class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def hello
    render text: "In the fullness of time, this will be a grid solver application that generates a four-by-four grid of random letters, then produces a list of all words that can be created from that grid. The rules are that you have to be able to connect the tiles the letters appear in (using adjacency, including diagonals), and each tile can be used only once."
  end

end
