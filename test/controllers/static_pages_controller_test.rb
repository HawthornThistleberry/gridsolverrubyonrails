require 'test_helper'

class StaticPagesControllerTest < ActionController::TestCase
  test "should get home" do
    get :home
    assert_response :success
    assert_select "title", "Grid Solver"
  end

  test "should get about" do
    get :about
    assert_response :success
    assert_select "title", "About · Grid Solver"
  end

  test "should get answer" do
    get :answer
    assert_response :success
    assert_select "title", "Answer · Grid Solver"
  end

end
